from htmlelement import HTMLAnchor, HTMLElement, HTMLHeading, HTMLPage

from wimweb import data

def page(title):
    page = HTMLPage()
    meta = HTMLElement('meta')
    meta.attributes['charset'] = 'utf-8'
    page.head.add(meta)

    title_tag = HTMLElement('title', [title])
    page.head.add(title_tag)
    page.add(HTMLHeading(1, title))

    return page

def reference_link(title):
    url = data.get_reference(title).get_page()
    return HTMLAnchor(url, title)

def refer(*sources):
    text = " & ".join(str(reference_link(source)) for source in sources)
    return '&lt;(pe {})'.format(text)

def grammar(info):
    return info
