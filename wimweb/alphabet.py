import re

# The order that we sort in the alphabet.
order = ' kruelopŋsxaiftwmnjh0123'
_reverse_order = { char: index for index, char in enumerate(order) }
assert len(order) == len(_reverse_order) # make sure the order has no duplicates
# There are characters that we collate together, we canonize them here:
duplicates = {
    'g': 'k',
    'b': 'p',
    'd': 't',
    'K': 'k',
}
for duplicate, original in duplicates.items():
    _reverse_order[duplicate] = _reverse_order[original]
# Upper case maps to lower case:
for char, index in list(_reverse_order.items()): # collect current items in list since we will modify the dict
    upper = char.upper()
    if upper != char:
        _reverse_order[upper] = index

def order_lookup(char):
    """Give a key such that the character is sorted according to the order.
    
    The key is an int n such that 0 <= n <= len(order).
    """
    try:
        return _reverse_order[char]
    except KeyError:
        # If the symbol is not in the order, we sort it as the last one.
        return len(order)

def natural_key(key):
    """Give a key such that the text sorts naturally according to the order.
    
    The key can be a string, or any iterable of iterables of ... of strings.
    """
    if not isinstance(key, str):
        return [natural_key(k) for k in key]

    def convert(c):
        if c.isdigit():
            return len(order) + 1 + int(c) # disambiguate from order_lookup
        else:
            return [order_lookup(char) for char in c]
    # Split the text at each group of digits,
    # and convert each group of (non-)digits to the correct key.
    return [convert(c) for c in re.split('(\d+)', key)]

def natural_sorted(iterable):
    return sorted(iterable, key=natural_key)
