#!/usr/bin/env python3

import argparse
from pathlib import Path
import sys

from htmlelement import HTMLParagraph

from wimweb import alphabet
from wimweb import data
from wimweb import presentation

parser = argparse.ArgumentParser(description='Generate WIM-web pages.')
parser.add_argument('-o', '--out', required=True, help='output directory')

args = parser.parse_args()
out_dir = Path(args.out)

try:
    out_dir.mkdir()
except FileExistsError:
    if out_dir.is_directory():
        print('Error: directory {} already exists.'.format(out_dir))
    else:
        print('Error: file {} already exists (and isn\'t even a directory!).'.format(out_dir))
    raise

for title, ref in data.get_references():
    page = presentation.page(title)
    page.add(HTMLParagraph(' '.join(str(html) for html in ref.info)))
    ref_path = ref.get_path(out_dir)
    with ref_path.open('w', encoding='utf-8') as reference_file:
        reference_file.write(str(page))

main_page = presentation.page('(welwester amisxular mendausilen)')
main_page.add(HTMLParagraph('{ref} (bonobontiptes amisxulfi weŋ balaselen / {order})'.format(
    ref=presentation.refer('WIM\ 0', 'WIM\ 2'),
    order=alphabet.order,
)))
main_page.add(data.render_all_words())

index_path = out_dir / 'index.html'
with index_path.open('w', encoding='utf-8') as index:
    index.write(str(main_page))
