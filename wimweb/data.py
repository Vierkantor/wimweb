from collections import namedtuple

from htmlelement import HTMLDiv, HTMLParagraph

from wimweb.alphabet import natural_sorted
from wimweb.presentation import grammar, refer

_words = {}

def word(name, *info):
    _words[name] = info
def render_all_words():
    all_words = HTMLDiv()
    for name, info in natural_sorted(_words.items()):
        all_info = ' '.join(str(html) for html in info)
        lemma = HTMLParagraph('({name} / {info})'.format(
            name=name,
            info=all_info,
        ))
        all_words.add(lemma)

    return all_words

def counting_word(name, ref, value, *extra):
    return word(name, ref, grammar('kiŋkat tultit'), '({})'.format(value), *extra)

class Reference:
    def __init__(self, title, identifier, *info):
        self.title = title
        self.identifier = identifier
        self.info = info
    def get_path(self, out_dir):
        return out_dir / self.get_page()
    def get_page(self):
        return self.identifier + '.html'

_references = {}
def reference(title, identifier, *info):
    _references[title] = Reference(title, identifier, *info)
def get_reference(title):
    return _references[title]
def get_references():
    for title, ref in _references.items():
        yield title, ref
def reference_wim(page, description, *info):
    title = 'WIM\ {page}/ {description}'.format(page=page, description=description)
    identifier = 'wim-{page}'.format(page=page)
    return reference(title, identifier, *info)

reference_wim(0)
reference_wim(1)
reference_wim(2)
reference_wim(3)
reference_wim(10)
reference_wim(11)
reference_wim(12)
reference_wim(13)

word('WIM\\', '(so am WIM\ : welwester amisxular mendausilen')
word('bonobonti', refer('WIM\ 3'), grammar('kiŋkat fi'), '(etfi isxulfi)', '(etfi so kruelopŋsxaiftwmnjh)')
word('isxul', refer('WIM\ 3'), grammar('ultam 2'), '(et pelcop menofi u permes mup etse husaxtefi u)')
word('sai', refer('WIM\ 3'), grammar('gulmat'), '(ot weŋ)')
word('weŋ', refer('WIM\ 3'), grammar('gulmat'), '(peretlen ŋecen)')
word('am', refer('WIM\ 3'), grammar('kiŋkat solen am'), '(etsetes sewadesla)')
word('wai', refer('WIM\ 3'), grammar('uspasit'), '(semenofit watevbenfi ek watevbenbet)', '(: &)')
word('plex', refer('WIM\ 11'), grammar('kiŋkat ar'), '(ping muplen menoarpen)')
word('besbul', refer('WIM\ 11'), grammar('kiŋkat ar'), '(plex (piŋ) wai pespulmenoarpen menoarpen)')
word('flasi', refer('WIM\ 11'), grammar('ultam 2'), '(plex menoar u)')
word('ot', refer('WIM\ 11'), grammar('un kiŋkat & gulmat'), '(defetpen _0len semeno)')
word('se', refer('WIM\ 11'), grammar('kiŋkat solen se'), '(etsetes wadeslase)')
word('mes', refer('WIM\ 11'), grammar('ultam 1'), '(etfi tsarn fi) (etfi sut fites)')
word('tes', refer('WIM\ 11'), grammar('ultam 2'), '(etfi so et fipen fimes)')
word('pe', refer('WIM\ 11'), grammar('un kiŋkat'), '(ot so)', '(so ~> ot so)')
word('husaxte', refer('WIM\ 11'), grammar('ultam 2'), '(etfi isxullfilen sankisfi u mup weswal kajla u)', '(& etfi weswul filen kajla)')
word('me', refer('WIM\ 13'), grammar('un kiŋkat & gulmat'), '(ot ot)')
counting_word('ost', refer('WIM\ 0', 'WIM\ 13'), 0)
counting_word('ek', refer('WIM\ 0', 'WIM\ 13'), 1)
counting_word('po', refer('WIM\ 0', 'WIM\ 13'), 2)
counting_word('ten', refer('WIM\ 0', 'WIM\ 13'), 3)
counting_word('wi', refer('WIM\ 0', 'WIM\ 13'), 10)
counting_word('fos', refer('WIM\ 0', 'WIM\ 13'), 11)
counting_word('spet', refer('WIM\ 0', 'WIM\ 13'), 12)
counting_word('sek', refer('WIM\ 0', 'WIM\ 13'), 13)
counting_word('kesa', refer('WIM\ 0'), 20)
counting_word('posi', refer('WIM\ 0'), 21)
counting_word('meit', refer('WIM\ 0'), 22)
counting_word('nemen', refer('WIM\ 0'), 23)
counting_word('sup', refer('WIM\ 0'), 30)
counting_word('welk', refer('WIM\ 0'), 31)
counting_word('testof', refer('WIM\ 0'), 32)
counting_word('exof', refer('WIM\ 0'), 33)
counting_word('huk', refer('WIM\ 0'), 100)
counting_word('skast', refer('WIM\ 0'), 211)
counting_word('truns', refer('WIM\ 0'), 10000)
