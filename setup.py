from setuptools import setup

setup(name='wimweb',
        version='0.1',
        description='welwester amisxular mendausilen',
        url='https://gitlab.com/Vierkantor/wimweb',
        author='Vierkantor',
        author_email='vierkantor@vierkantor.com',
        license='AGPL',
        packages=['wimweb'],
        dependency_links=['https://gitlab.com/Vierkantor/htmlelement/-/archive/master/htmlelement-master.tar#egg=htmlelement-0.1'],
        install_requires=[
            'htmlelement',
        ],
        zip_safe=False)

